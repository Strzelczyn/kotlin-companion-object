class MyClass {
    companion object {
        var myText: String = "Hi"
    }
}

fun main(args: Array<String>) {
    print(MyClass.myText)
}
